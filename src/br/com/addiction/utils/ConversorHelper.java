package br.com.addiction.utils;

import java.util.Calendar;

public class ConversorHelper {
	
	public long converteParaHoras(long tempoEmMilisegundos){
		tempoEmMilisegundos = tempoEmMilisegundos/3600000;
		return tempoEmMilisegundos;		
	}	
	
	public long converteParaminutos(long tempoEmMilisegundos){
		tempoEmMilisegundos = tempoEmMilisegundos/60000;
		return tempoEmMilisegundos;		
	}	
	
	public long converteParasegundos(long tempoEmMilisegundos){
		tempoEmMilisegundos = tempoEmMilisegundos/60;
		return tempoEmMilisegundos;		
	}	
}
