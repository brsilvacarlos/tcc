package br.com.addiction.servico;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Toast;
import br.com.addiction.R;

public class ScreenReceiver extends BroadcastReceiver {
    
    public static boolean wasScreenOn = true;

    @Override
    public void onReceive(final Context context, final Intent intent) {
    Log.e("test","onReceive");
    Toast.makeText(context, "Received", Toast.LENGTH_SHORT).show();
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // do whatever you need to do here
            wasScreenOn = false;
            Log.e("test","wasScreenOn"+wasScreenOn);
            Toast.makeText(context, "Screen Off", Toast.LENGTH_SHORT).show();
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // and do whatever you need to do here
            wasScreenOn = true;
            Log.e("test","wasScreenOn"+wasScreenOn);
            Toast.makeText(context, "Screen On", Toast.LENGTH_SHORT).show();
        }else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
            Log.e("test","userpresent");
            Toast.makeText(context, "User Present", Toast.LENGTH_SHORT).show();
        }
    }
}
