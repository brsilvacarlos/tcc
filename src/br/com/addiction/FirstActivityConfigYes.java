package br.com.addiction;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;
import br.com.addiction.model.ApplicationAdapter;

public class FirstActivityConfigYes extends ListActivity {
	private PackageManager packageManager = null;
	private List<ApplicationInfo> applist = null;
	private ApplicationAdapter listadaptor = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_activity_config_yes);

		packageManager = getPackageManager();

		new LoadApplications().execute();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = true;

		switch (item.getItemId()) {
		/*
		 * case R.id.menu_about: { displayAboutDialog();
		 * 
		 * break; }
		 */
		default: {
			result = super.onOptionsItemSelected(item);
		}
		}

		return result;
	}

	private void displayAboutDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// builder.setTitle(getString(R.string.about_title));
		// builder.setMessage(getString(R.string.about_desc));

		builder.setPositiveButton("Know More",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent browserIntent = new Intent(Intent.ACTION_VIEW,
								Uri.parse("http://javatechig.com"));
						startActivity(browserIntent);
						dialog.cancel();
					}
				});
		builder.setNegativeButton("No Thanks!",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		builder.show();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		ApplicationInfo app = applist.get(position);
//		try {
			//Código utilizado para iniciar uma intent para o app selecionado na lista
			
//			Intent intent = packageManager
//					.getLaunchIntentForPackage(app.packageName);
//
//			if (null != intent) {
//				startActivity(intent);
			
			
			Toast.makeText(FirstActivityConfigYes.this,
					"posição da lista é: " + (position + 1),
					Toast.LENGTH_LONG).show();
			//Ao clicar sobre o item, seleciona seu checkbox
			CheckBox itemCheckBox = (CheckBox) v.findViewById(R.id.checkBox);
			itemCheckBox.setChecked(!itemCheckBox.isChecked());
			
//			}
//		} catch (ActivityNotFoundException e) {
//			Toast.makeText(FirstActivityConfigYes.this, e.getMessage(),
//					Toast.LENGTH_LONG).show();
//		} catch (Exception e) {
//			Toast.makeText(FirstActivityConfigYes.this, e.getMessage(),
//					Toast.LENGTH_LONG).show();
//		}
	}

	private List<ApplicationInfo> checkForLaunchIntent(
			List<ApplicationInfo> list) {
		ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
		for (ApplicationInfo info : list) {
			try {
				if (null != packageManager
						.getLaunchIntentForPackage(info.packageName)) {
					applist.add(info);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return applist;
	}

	private class LoadApplications extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress = null;

		@Override
		protected Void doInBackground(Void... params) {
			applist = checkForLaunchIntent(packageManager
					.getInstalledApplications(PackageManager.GET_META_DATA));
			listadaptor = new ApplicationAdapter(FirstActivityConfigYes.this,
					android.R.layout.simple_list_item_multiple_choice, applist);

			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(Void result) {
			setListAdapter(listadaptor);
			progress.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(FirstActivityConfigYes.this, null,
					"Loading application info...");
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}
}
