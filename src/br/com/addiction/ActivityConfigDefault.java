package br.com.addiction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ActivityConfigDefault extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.config_default);		
		
		Button irParaMain = (Button) findViewById(R.id.irParaMain);
		irParaMain.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intentIrParaMain = new Intent(ActivityConfigDefault.this, MainActivity.class);
				startActivity(intentIrParaMain);			
				finish();
			}
		});
	}
}
