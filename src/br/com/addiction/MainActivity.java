package br.com.addiction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;
import br.com.addiction.servico.testeServico;
import br.com.addiction.utils.ConversorHelper;

public class MainActivity extends Activity {
	
	ConversorHelper conversor = new ConversorHelper();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		//inicia serviço identifica bloqueio de tela
		startService(new Intent(getApplicationContext(), testeServico.class));	
		
		ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		 List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

		 long currentMillis = Calendar.getInstance().getTimeInMillis();
		 Calendar cal = Calendar.getInstance();

		 for (ActivityManager.RunningServiceInfo info : services) {
		     cal.setTimeInMillis(currentMillis-info.activeSince);
		     
		     long horas = conversor.converteParaHoras(info.activeSince);
		     long minutos = conversor.converteParaminutos(info.activeSince);
		     long segundos = conversor.converteParasegundos(info.activeSince);
		     

		     Log.i("tempo", String.format("Process %s with component %s has been running since %s (%2d : %2d : %2d)",
		             info.process, info.service.getShortClassName(), cal.getTime().toString(), horas, minutos, segundos));
		 }		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		//ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		//ActivityManager.RunningServiceInfo servico = (RunningServiceInfo) activityManager.getRunningServices(Integer.MAX_VALUE);
		
		SimpleDateFormat sdf = new SimpleDateFormat();
		Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
		String dataFormatada = sdf.format(hora);
		
//		Toast.makeText(MainActivity.this,"A hora atual é: " + dataFormatada, Toast.LENGTH_LONG).show();		
	}
	
	//Carrega o menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}	
}
