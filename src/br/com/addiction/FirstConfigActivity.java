package br.com.addiction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstConfigActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first_config);
		
		Button botaoNao = (Button) findViewById(R.id.botao_nao);
		botaoNao.setOnClickListener(new OnClickListener() {
			
			//botão não apertado. Pula as configurações
			@Override
			public void onClick(View arg0) {
				Intent configDefault = new Intent(FirstConfigActivity.this, ActivityConfigDefault.class);
				startActivity(configDefault);		
			}
		});	
		
		Button botaosim = (Button) findViewById(R.id.botao_sim);
		botaosim.setOnClickListener(new OnClickListener() {
			
			//botão sim apertado. Vai para as configurações iniciais.
			@Override
			public void onClick(View v) {
				Intent irParaConfig = new Intent(FirstConfigActivity.this, FirstActivityConfigYes.class);
				startActivity(irParaConfig);			
			}
		});		
	}
}
